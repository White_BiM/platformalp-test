import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Dialogue from '@/components/Dialogue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        { path: '/', redirect: '/home' },
        {
            path: '/home',
            props: true,
            name: 'Home',
            component: Home
        },
        {
            path: '/home/dialogue/:id',
            props: true,
            name: 'Dialogue',
            component: Dialogue
        },
    ],
    scrollBehavior(){
        return{x:0,y:0}
    }
})