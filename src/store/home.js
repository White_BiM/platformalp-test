
const Dialogues = [
    {
        id: 1,
        subject: 'Простой запрос',
        created: '2019-08-01 23:59',
        parts: [
            {
                id: 1,
                author: 'vasya',
                text: 'Привет, как дела?',
                created: '2019-08-01 23:59',
            },
            {
                id: 2,
                author: 'petya',
                created: '2019-08-02 01:20',
                text: 'Привет, все хорошо, спасибо!',
            },
            {
                id: 3,
                author: 'petya',
                created: '2019-08-02 05:20',
                text: 'А у тебя?',
            },
        ],
    },
    {
        id: 2,
        subject: 'Вопрос по домену',
        created: '2016-03-02 14:19',
        parts: [
            {
                id: 1,
                author: 'petr',
                created: '2019-08-06 12:20',
                text: 'Здравствуйте, тут есть кто-нибудь?',
            },
            {
                id: 2,
                author: 'vasiliy',
                created: '2019-08-06 12:34',
                text: 'Да, я вас слушаю!',
            },
            {
                id: 3,
                author: 'petr',
                created: '2019-08-06 12:38',
                text: 'Помогите мне настроить домен!',
            },
        ],
    },
]

export default {
    state: {
        dialogues: [],
    },
    mutations: {
        setDialogues(state, payload){
            state.dialogues = payload
        },
        addMessage(state, payload){
            let dialogue = state.dialogues.find(dialogue => dialogue.id === payload.id)
            dialogue.parts.push(payload.newMessage)
        },
    },
    actions: {
        async getDiallogues({commit}){
            try{
                return await new Promise(resolve => {
                    setTimeout(() => {
                        resolve()
                        commit('setDialogues', Dialogues)
                    }, 3000)
                })
            } catch(error){
                throw error
            }
        },
        async sendMessage({commit}, payload){
            try{
                const id = this.getters.dialogueById(payload.dialogueId).parts.length + 1
                const newMessage = {
                    id,
                    text: payload.message,
                    created: new Date().toLocaleString('ru')
                }
                return await new Promise(resolve => {
                    setTimeout(() => {
                        resolve()
                        commit('addMessage', {newMessage, id: payload.dialogueId})
                    }, 3000)
                })
            } catch(error){
                throw error
            }
        },
},

    getters:  {
        dialogues (state) {
            return state.dialogues
        },
        dialogueById(state) {
            return dialogueId => {
                return state.dialogues.find(dialogue => dialogue.id === dialogueId)
            }
        }
    }
}