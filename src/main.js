import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex';
import VueRouter from 'vue-router'
import router from './router'
import store from './store'
import moment from 'moment'
import 'moment/locale/ru'

moment.locale('ru-RU')
Vue.config.productionTip = false
Vue.use(Vuex, VueRouter );
new Vue({
    render: h => h(App),
    router,
    store,
}).$mount('#app')
